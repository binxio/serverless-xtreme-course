<!-- .slide: data-background="../assets/diving-1600668.jpg" class="whiter" -->
# Serverless X-Treme Course

Welcome

---

## Course overview

This 1 day course is designed to give experienced developers the know-how to confidently start creating serverless
solutions in the AWS Cloud. The course ensures you will have a solid understanding of AWS serverless components, 
the tooling and the development process. 

---

## Objectives

- Know and be able to create serverless solutions using Infrastructure as code,
- Know how to use the AWS Cloud and the supporting tooling,
- Know how to create S3 buckets, static websites, and deploy websites using CloudFront,
- Know how to create Serverless APIs using AWS API Gateway
- Know how to create a full application using the tombstone project 'Tour of Heroes' 

---

## Target Audience

- Application developers, software architects and business analysts wishing to learn about AWS Serverless. 

---

## Day 1 (AM)
- Introductions
- Mindmap: [CloudFormation](https://www.mindomo.com/nl/mindmap/cloudformation-86c393e3175143dbb66532af0a743f6b)
- Mindmap: [IAM](https://www.mindomo.com/nl/mindmap/improved-iam-c4e864ab309346778dc38d729bd33aaf)
- Handson: Account setup
- Mindmap: [S3](https://www.mindomo.com/nl/mindmap/s3-393a6a16eeae452bbd320bd264190da1)
- Handson: S3
- Mindmap: [Api Gateway](https://www.mindomo.com/nl/mindmap/api-gateway-ca81c1bf4a694cfeb40c47aa19af9b6d) and [AWS Lambda](https://www.mindomo.com/nl/mindmap/lambda-c70b7571a15348f3a01658899fed5a49)
- Handson: API Gateway

---

## Day 1 (PM)
- Mindmap: [CloudFront](https://www.mindomo.com/nl/mindmap/cloudfront-a05202d51d20480ba5f15c69f947f6fd)
- Handson: CloudFront
- Mindmap: [Serverless](https://www.mindomo.com/nl/mindmap/overview-e86dbbe27702429882d0edccf131baa8)
- Handson: Tour of Heroes

---

## Introductions

- Instructor Introduction

- Student Introductions
  - Your Name and Company
  - Where you Live
  - Your Job Role
  - Any experience with the cloud?
  - What do you expect to get out of this class?

---

## Class Logistics

- Start and end times
- Breaks and lunch
- Topics not on the agenda
- Assignments completed

---

## Serverless Definition

- No management of servers
- Cost based on usage
- High available   
- Auto-scale and auto-provision based on load

---

## Mindmap CloudFormation

- [AWS Overview](https://www.mindomo.com/nl/mindmap/overview-e86dbbe27702429882d0edccf131baa8)
- [AWS CloudFormation](https://www.mindomo.com/nl/mindmap/cloudformation-86c393e3175143dbb66532af0a743f6b)

---

## Mindmap IAM

- [AWS Overview](https://www.mindomo.com/nl/mindmap/overview-e86dbbe27702429882d0edccf131baa8)
- [AWS IAM](https://www.mindomo.com/nl/mindmap/improved-iam-c4e864ab309346778dc38d729bd33aaf)

---

## Handson IAM

awscli installation:

- [Linux Installation](https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-linux.html)
- [OSX Installation](https://docs.aws.amazon.com/cli/latest/userguide/cli-install-macos.html)
- [Windows Installation](https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-windows.html)

When using [homebrew](https://brew.sh/):

```text
$ brew install awscli
```

---

## Setup Tools

- Install [Python](https://www.python.org/) and pip (python installs packages)
- [sceptre](https://sceptre.cloudreach.com/latest/): `pip install sceptre or brew install sceptre`
- [aws-login](https://github.com/binxio/aws-login): `pip install aws-login`
- [aws-cfn-update](https://github.com/binxio/aws-cfn-update): `pip install aws-cfn-update`
- [httpie](https://httpie.org/): `pip install httpie or brew install httpie`

---

## Configure the awscli

```text
$ aws configure --profile xtreme-root
AWS Access Key ID [None]: <your-access-key>
AWS Secret Access Key [None]: <your-secret-access-key>
Default region name [None]: eu-west-1
Default output format [None]: json
```

---

## Testing the awscli

```text
$ aws sts get-caller-identity --profile xtreme-root
{
    "Account": "1234567890", 
    "UserId": "YOUR_USER_ID", 
    "Arn": "arn:aws:iam::1234567890:user/your-user-name"
}
```

---

## Setup the account

- `git clone git@gitlab.com:binxio/serverless-xtreme-project.git`
- edit `01-iam/templates/create-user-and-roles.yaml`
- edit `UserName` and set it to your email address
- make create

---

## Setup MFA

- login with your account number

```text
$ aws sts get-caller-identity --profile xtreme-root
```

- IAM username: the one you set in the CloudFormation template

- get your temporary password:

```text
$ make describe
```

---

## Setup MFA

- Login 'http://console.aws.amazon.com' with your username and password
- Navigate to IAM -> Users -> <your username>
- Security Credentials -> Assigned MFA device -> Manage
- Virtual MFA Device -> Show QR Code
- Sign out

---

## Change password

- [Login](http://console.aws.amazon.com) with your username and password
- Use your MFA device
- Navigate to IAM -> Users -> <your username>
- Security Credentials -> Console password -> Manage
- Set password -> custom password -> type a 6 char password
- Sign out

---

## Create access keys

- [Login](http://console.aws.amazon.com) with your username and password
- Use your MFA device
- Navigate to IAM -> Users -> <your username>
- Access Keys -> Create Access Key
- Download .csv File
- Show the access keys

```text
$ aws configure --profile xtreme
AWS Access Key ID [None]: <your-access-key>
AWS Secret Access Key [None]: <your-secret-access-key>
Default region name [None]: eu-west-1
Default output format [None]: json

$ aws sts get-caller-identity --profile xtreme
```

---

## Assume a role in the AWS Console

- [Login](http://console.aws.amazon.com) with your username and password
- Use your MFA device
- Top right, click on your username
- Switch role
- Type your account number
- Type the rolename `Administrator`
- Type a name
- Click 'Switch role'

---

## Setup temporary credentials

Use `aws-login`:

```text
$ aws-login mfa -s xtreme -t xtreme-deploy -R 43200

$ aws-login mfa --source-profile xtreme --target-profile xtreme-deploy --role-expiration 43200

$ aws sts get-caller-identity --profile xtreme-deploy
``` 

---

## Mindmap S3

- [AWS Overview](https://www.mindomo.com/nl/mindmap/overview-e86dbbe27702429882d0edccf131baa8)
- [AWS S3](https://www.mindomo.com/nl/mindmap/s3-393a6a16eeae452bbd320bd264190da1)
- [AWS Lambda](https://www.mindomo.com/nl/mindmap/lambda-c70b7571a15348f3a01658899fed5a49)

---

## Handson S3 - Bucket

- Create a simple bucket: `01-s3/01-bucket` 
- read the assignment in `readme.md`
- look at `templates/bucket.yaml`
- read [AWS::S3::Bucket](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-s3-bucket.html)
- read `aws s3 help`
- type `make help`
- type `make deploy`
- type `make describe`

---

## Handson S3: Static Website

- Create a static website: `01-s3/02-static-website` 
- read the assignment in `readme.md`
- look at `templates/static-website.yaml`
- read [AWS::S3::Bucket](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-s3-bucket.html)
- read `aws s3 help`
- type `make deploy`
- type `make describe`

---

## Handson S3: Bucket with Lambda

- Create a static website: `01-s3/03-bucket-with-lambda` 
- read the assignment in `readme.md`
- look at `templates/bucket-with-lambda.yaml`
- read [AWS::S3::Bucket](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-s3-bucket.html)
- read `aws s3 help`
- type `make deploy`
- type `make describe`

---

## Mindmap API Gateway

- [AWS Overview](https://www.mindomo.com/nl/mindmap/overview-e86dbbe27702429882d0edccf131baa8)
- [Api Gateway](https://www.mindomo.com/nl/mindmap/api-gateway-ca81c1bf4a694cfeb40c47aa19af9b6d)

---

## Handson API Gateway - Swagger

- Create a static website: `03-api-gateway/01-swagger` 
- read the assignment in `readme.md`
- look at `templates/api-gateway.yaml`
- type `make merge-swagger`
- type `make deploy`
- type `make describe`

---

## Handson API Gateway - Mock Integration

- Create a static website: `03-api-gateway/02-mock-integration` 
- read the assignment in `readme.md`
- look at `templates/api-gateway.yaml`
- type `make merge-swagger`
- type `make deploy`
- type `make describe`


---

## Handson API Gateway - HTTP Integration

- Create a static website: `03-api-gateway/03-http-integration` 
- read the assignment in `readme.md`
- look at `templates/api-gateway.yaml`
- type `make merge-swagger`
- type `make deploy`
- type `make describe`

---

## Handson API Gateway - Lambda Integration

- Create a static website: `03-api-gateway/04-lambda-integration` 
- read the assignment in `readme.md`
- look at `templates/api-gateway.yaml`
- type `make merge-swagger`
- type `make deploy`
- type `make describe`

---

## Mindmap API Gateway

- [AWS Overview](https://www.mindomo.com/nl/mindmap/overview-e86dbbe27702429882d0edccf131baa8)
- [CloudFront](https://www.mindomo.com/nl/mindmap/cloudfront-a05202d51d20480ba5f15c69f947f6fd)

---

## Handson API Gateway - CloudFront

- Create a static website: `04-cloudfront` 
- read the assignment in `readme.md`
- look at `templates/cloudfront.yaml`
- type `make deploy`
- type `make describe`

---

## Mindmap DynamoDB

- [AWS Overview](https://www.mindomo.com/nl/mindmap/overview-e86dbbe27702429882d0edccf131baa8)
- [DynamoDB](https://www.mindomo.com/nl/mindmap/dynamodb-c17c67b3ffb44c259c341837a86ea855)

---

## Tour of Heroes

- Create a static website: `05-tour-of-heroes` 
- read the assignment in `readme.md`
- look at `templates/tour-of-heroes.yaml`
- type `make merge-swagger`
- type `make deploy`
- type `make describe`

---

<!-- .slide: data-background="diving-1600668.jpg" class="whiter" -->

# Binx.io

End