function(hljs) {
  return {
    aliases: ['jinja2', 'jinja'],
    case_insensitive: true,
    contains: [
      {
        className: 'string',
        begin: '{{', 
        end: '}}'
      }
    ]
  }
}
